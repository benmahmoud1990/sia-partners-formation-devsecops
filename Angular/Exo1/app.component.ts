import { Component, OnInit, ElementRef, HostListener, ViewChild } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
url='';
@ViewChild('myDiv') divElement!: ElementRef;
onClick() {
   this.link = this.sanitizer.bypassSecurityTrustUrl(this.url);
}
  link:any;
  constructor(public sanitizer: DomSanitizer) {
  this.url = 'https://google.fr';
  this.link=this.url; }
  ngOnInit() {}

}
